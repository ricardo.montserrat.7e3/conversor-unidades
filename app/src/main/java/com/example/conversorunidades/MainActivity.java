package com.example.conversorunidades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
{

    private double calculate(EditText input, Spinner list)
    {
        double result = 0;
        String userValue = input.getText().toString();
        if (userValue.isEmpty())
            Toast.makeText(MainActivity.this, "There is no value to convert!", Toast.LENGTH_SHORT).show();
        else
        {
            switch (list.getSelectedItemPosition())
            {
                case 0:
                    result = Double.parseDouble(userValue) * 2.54; break; //De polzada a centímetre
                case 1:
                    result = Double.parseDouble(userValue) * 0.9144; break; //De iarda a metre
                case 2:
                    result = Double.parseDouble(userValue) * 1.60934; break; //De milla a quilòmetre
                case 3:
                    result = Double.parseDouble(userValue) * 0.393701; break; //De centímetre a polzada
                case 4:
                    result = Double.parseDouble(userValue) * 1.09361; break; //De metre a iarda
                case 5:
                    result = Double.parseDouble(userValue) * 0.621371; break; //De quilòmetre a milla
            }
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        final EditText input = findViewById(R.id.input);
        final Spinner list = findViewById(R.id.array);
        final TextView resultText = findViewById(R.id.resultText);
        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (resultText.getVisibility() != View.VISIBLE)
                    resultText.setVisibility(View.VISIBLE);
                resultText.setText(String.format(Locale.ENGLISH, "%.3f", calculate(input, list)));
            }
        });

        input.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (keyCode == 66)
                {
                    if (resultText.getVisibility() != View.VISIBLE)
                        resultText.setVisibility(View.VISIBLE);
                    resultText.setText(String.format(Locale.ENGLISH, "%.3f", calculate(input, list)));
                }
                return false;
            }
        });
    }
}